﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour {

	public Cell currentCell;

	// Use this for initialization
	void Start () {
	}

	void OnDrawGizmos(){
		
		RaycastHit rayHit;
		Physics.Linecast (transform.position, transform.position - transform.up,out rayHit);
		if (rayHit.transform != null) {
			currentCell = rayHit.transform.GetComponent<Cell> ();
			Gizmos.color = Color.red;
		} else {
			currentCell = null;
			Gizmos.color = Color.white;
		}
		Gizmos.DrawLine (transform.position,transform.position-transform.up);
	}

	public List<Cell> GetOnRange(int range){
		if (currentCell == null) {
			//Debug.LogError ("wtf");
			return new List<Cell> ();
		}
		Queue<Cell> cells = new Queue<Cell>();
		List<Cell> result = new List<Cell>();
		Dictionary<Cell,int> rangeMap = new Dictionary<Cell, int>();
		rangeMap [currentCell] = 0;
		currentCell.range = 0;
		cells.Enqueue (currentCell);
		while (cells.Count != 0) {
			Cell current = cells.Dequeue ();
			if (rangeMap [current] == range) {
				continue;
			}
			foreach(var i in current.Neighbors){
				int t;
				if (!rangeMap.TryGetValue(i,out t)){
					rangeMap [i] = rangeMap [current] + 1;
					i.range = rangeMap [i];
					cells.Enqueue (i);
				}
			}
			result.Add (current);
			//Debug.Log (current);
		}

		return result;
	}
	public int range = 2;
	// Update is called once per frame
	void Update () {
		//TODO remove this debug
		foreach (var i in GetOnRange (range)) {
			Debug.DrawLine (transform.position, i.transform.position,Color.yellow);
		}

	}
}
