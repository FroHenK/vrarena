﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Cell : MonoBehaviour,ICell
{
    public Unit CurrentUnit;
	public int range;//for graph
    public Unit GetCurrentUnit()
    {
        return CurrentUnit;
    }
    public List<ICell> GetNeighbors()
    {
        List<ICell> ls = new List<ICell>();
        foreach (Cell c in Neighbors)
        {
            ls.Add(c);
        }
        return ls;
    }

    public int Ways;
    public List<Cell> Neighbors;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnDrawGizmos()
    {
        List<Cell> nei=new List<Cell>();
        for (int i = 0; i < Ways; i++)
        {
            Ray ray = new Ray(transform.position, Quaternion.AngleAxis(360 / Ways * i, transform.up) * (transform.forward));
            Gizmos.color = Color.white;
            RaycastHit hit;
            if (Physics.Raycast(ray,out hit,maxDistance: 1))
            {
                if (hit.rigidbody != null)
                {
                    var c = hit.rigidbody.gameObject.GetComponent<Cell>();
                    if ( c!= null)
                    {
                        Gizmos.color = Color.red;
                        nei.Add(c);
                    }
                }
            }
            Gizmos.DrawRay(ray);
        }
        if (nei != Neighbors)
        {
            Neighbors = nei;
        }

    }

}
