﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
public class GlobalController : MonoBehaviour {
    public Transform GridTransform;
    public List<Cell> GetCellsList()
    {
        
        if (cells == null)
        {
            cells = new List<Cell>();
            cells.AddRange(GridTransform.GetComponentsInChildren<Cell>());
        }
        return cells;
    }
	List<Cell> cells;

	// Use this for initialization
	void Start () {
		//cells = GameObject.Find ("Grid");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
